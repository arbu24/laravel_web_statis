<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>HTML - Form</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <form action="/welcome" method="POST">
        @csrf
        <h3>Sign Up Form</h3>
        <label for="first_name">First name:</label> <br>
        <br>
        <input type="text" name="first_name" id="first_name"> <br>
        <br>
        <label for="last_name">Last name:</label> <br>
        <br>
        <input type="text" name="last_name" id="last_name"> <br>
        <br>
        <label>Gender:</label> <br>
        <br>
        <input type="radio" name="gender" value="male"> Male <br>
        <input type="radio" name="gender" value="female"> Female <br>
        <input type="radio" name="gender" value="oth"> Other <br>
        <br>
        <label>Language Spoken:</label> <br>
        <br>
        <select name="nationality">
            <option value="ind"> Indonesian</option>
            <option value="sng"> Singaporean</option>
            <option value="mly"> Malaysian</option>
            <option value="amc"> American</option>
        </select> <br><br>
        <label>Gender:</label><br>
        <br>
        <input type="checkbox" name="ind"> Bahasa Indonesia <br>
        <input type="checkbox" name="eng"> English <br>
        <input type="checkbox" name="oth"> Other <br>
        <br>
        <label>Bio:</label> <br><br>
        <textarea name="bio" cols="30" rows="10"></textarea> <br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>